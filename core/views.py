from django.urls import reverse
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from core import models, forms


class StudentList(ListView):
    model = models.Student
    template_name = 'core/student_list.html'


class StudentDetail(DetailView):
    model = models.Student
    template_name = 'core/student_detail.html'
    context_object_name = 'student'


class StudentCreate(CreateView):
    model = models.Student
    template_name = 'core/student_create.html'
    form_class = forms.StudentForm

    def get_success_url(self) -> str:
        return reverse('core:students')


class StudentUpdate(UpdateView):
    model = models.Student
    template_name = 'core/student_update.html'
    form_class = forms.StudentForm

    def get_success_url(self) -> str:
        return reverse('core:students')


class StudentDelete(DeleteView):
    model = models.Student
    template_name = 'core/student_delete.html'

    def get_success_url(self) -> str:
        return reverse('core:students')
