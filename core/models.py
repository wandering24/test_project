from django.db import models


class Direction(models.Model):
    name = models.CharField('Название', max_length=255)


class Student(models.Model):
    first_name = models.CharField('Имя', max_length=255, blank=True)
    last_name = models.CharField('Фамилия', max_length=255, blank=True)
    direction = models.ForeignKey(
        Direction,
        on_delete=models.CASCADE,
        verbose_name='Направление',
        blank=True
    )

    class Meta:
        verbose_name = 'Студент'
        verbose_name_plural = 'Студенты'

    def __str__(self):
        return self.get_fullname()

    def get_fullname(self) -> str:
        return ' '.join(filter(bool, [self.last_name, self.first_name]))
