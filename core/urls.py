from django.urls import path
from core import views

app_name = 'core'

urlpatterns = [
    path('students/', views.StudentList.as_view(), name='students'),
    path('student/<int:pk>', views.StudentDetail.as_view(), name='student'),

    path('student/create', views.StudentCreate.as_view(), name='student_create'),
    path('student/update/<int:pk>', views.StudentUpdate.as_view(), name='student_update'),
    path('student/delete/<int:pk>', views.StudentDelete.as_view(), name='student_delete'),
]
