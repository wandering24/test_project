import factory
from faker import Factory

from core import models

factory_ru = Factory.create('ru-Ru')


class Direction(factory.django.DjangoModelFactory):
    name = factory_ru.word()

    class Meta:
        model = models.Direction


class Student(factory.django.DjangoModelFactory):
    first_name = factory_ru.first_name()
    last_name = factory_ru.last_name()
    direction = factory.SubFactory(Direction)

    class Meta:
        model = models.Student