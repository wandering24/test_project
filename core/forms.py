from django import forms

from core import models


class StudentForm(forms.ModelForm):
    first_name = forms.CharField(label='Имя', required=True)
    last_name = forms.CharField(label='Фамилия', required=True)
    direction = forms.ModelChoiceField(label='Направление', queryset=models.Direction.objects.all(), required=True)

    class Meta:
        model = models.Student
        fields = '__all__'
