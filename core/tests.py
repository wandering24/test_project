from django.test import TestCase, Client
from django.urls import reverse
from core import factories


class StudentTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.direction = factories.Direction()
        self.student = factories.Student(direction=self.direction)

    def test_list_data(self):
        response = self.client.get(reverse('core:students'))
        self.assertEqual(response.status_code, 200)

    def test_detail(self):
        response = self.client.get(reverse('core:student', kwargs={'pk': self.student.pk}))
        self.assertEqual(response.status_code, 200)

    def test_create(self):
        data = {
            'first_name': 'test',
            'last_name': 'test2',
            'direction': self.direction,
        }
        response = self.client.post(path=reverse('core:student_create'), data=data, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_get_fullname(self):
        test_fullname = f'{self.student.last_name} {self.student.first_name}'
        self.assertEqual(self.student.get_fullname(), test_fullname)


