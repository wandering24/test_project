from django.contrib import admin

from core import models


@admin.register(models.Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'direction',)


@admin.register(models.Direction)
class DirectionAdmin(admin.ModelAdmin):
    list_display = ('name',)
